#pragma once

#include "syscalls.hpp"

namespace services {
using namespace globals;

static auto portcheck(const char *symbol, u16 port) -> void {
  const auto s = socket(AF_INET, SOCK_STREAM, PROTO_TCP);
  if (s < 0) {
    *bptr++ = '?';
    return;
  }
  sockaddr addrinfo;
  addrinfo.sin_family = AF_INET;
  addrinfo.sin_addr   = 0x0100007f;
  addrinfo.sin_port   = __builtin_bswap16(port);
  auto r              = connect(s, &addrinfo, 16);
  close(s);

  if (r == 0) {
    while (*symbol) {
      *bptr++ = *symbol++;
    }
  }
}
static auto filecheck(const char *symbol, const char *path) -> void {
  char buffer[4096];
  if (stat(path, buffer) == 0) {
    while (*symbol) {
      *bptr++ = *symbol++;
    }
  }
}

[[noreturn]]
static inline auto run() -> void {
  portcheck("🇷🇺", 8888);
  portcheck("🇳🇱", 8889);
  portcheck("🧅", 9050);
  portcheck("🐋", 2375);
  portcheck("🔄", 8384);
  portcheck("🐘", 5432);
  portcheck("🦣", 5555);
  portcheck("🕸️", 80);
  portcheck("🙄", 3737);

  filecheck("⌛", "/run/chrony/chronyd.sock");

  write(1, buffer, bptr - buffer);
  exit();
}
} // namespace services
