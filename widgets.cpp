#include "syscalls.hpp"

#include "cpulevel.hpp"
#include "disk.hpp"
#include "draw.hpp"
#include "lo.hpp"
#include "maxtemp.hpp"
#include "net.hpp"
#include "rfkill.hpp"
#include "services.hpp"
#include "weather.hpp"
#include "zram.hpp"

[[gnu::noreturn]] auto main(void *rsp) -> void {
  u64   *argc = (u64 *)(rsp);
  char **argv = (char **)(argc + 1);
  if (*argc != 2) [[unlikely]] {
    static constexpr char msg[] = "Wrong argc";
    write(1, msg, sizeof(msg) - 1);
    exit();
  }

  const char type = argv[1][0];
  switch (type) {
  case 'a':
    zram();
    break;
  case 'b':
    rfkill();
    break;
  case 'c':
    draw::draw();
    break;
  case 'd':
    maxtemp::maxtemp();
    break;
  case 'e':
    net::net();
    break;
  case 'f':
    disk::disk();
    break;
  case 'g':
    lo::lo();
    break;
  case 'h':
    services::run();
    break;
  case 'i':
    cpulevel::cpulevel();
    break;
  case 'j':
    weather::weather();
    break;
  [[unlikely]] default:
    static constexpr char msg[] = "no such widget found";
    write(1, msg, sizeof(msg) - 1);
    exit();
  }

  exit();
}
