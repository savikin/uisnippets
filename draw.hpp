#pragma once

#include "lib.hpp"
#include "syscalls.hpp"

namespace draw {
using namespace lib;
using namespace globals;

static constexpr const auto e3 = 1'000;
static constexpr const auto e4 = 10'000;
static constexpr const auto e6 = 1'000'000;

[[noreturn]]
static auto draw() -> void {
  u64 mamps      = parse_from("/sys/class/power_supply/BAT1/current_now") / e3;
  u64 mvolts     = parse_from("/sys/class/power_supply/BAT1/voltage_now") / e3;

  u64 watts      = (mamps * mvolts) / e6;
  u64 fractional = ((mamps * mvolts) % e6) / e4;

  if (watts == 0 && fractional == 0) {
    exit();
  }

  bptr    = lib::writenum(watts, bptr);
  *bptr++ = '.';
  if (fractional < 10) {
    *bptr++ = '0';
  }
  bptr = lib::writenum(fractional, bptr);
  write(1, buffer, bptr - buffer);
  exit();
}
}; // namespace draw
