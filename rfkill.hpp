#pragma once

#include "lib.hpp"
#include "syscalls.hpp"

[[noreturn]]
static inline void rfkill() {
  using namespace lib;
  using namespace globals;

  static constexpr auto order = []() -> bool { return readchar("/sys/class/rfkill/rfkill0/type") == 'w'; };
  static constexpr auto state = [](const char *path) -> void {
    if (readchar(path) == '1') {
      bptr = lib::write(bptr, SIZEDSTR("🚀"));
    } else {
      bptr = lib::write(bptr, SIZEDSTR("x"));
    }
  };

  if (order()) {
    state("/sys/class/rfkill/rfkill0/state");
    state("/sys/class/rfkill/rfkill1/state");
  } else {
    state("/sys/class/rfkill/rfkill1/state");
    state("/sys/class/rfkill/rfkill0/state");
  }
  write(1, buffer, bptr - buffer);
  exit();
}
