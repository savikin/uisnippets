#pragma once

#include "lib.hpp"
#include "syscalls.hpp"

namespace maxtemp {
using namespace lib;
using namespace globals;

static u64 current;

static constexpr const char persist[]  = "/home/angel/.cache/waybar-maxtemp-status";
static constexpr const char zonetemp[] = "/sys/devices/LNXSYSTM:00/LNXSYBUS:01/LNXTHERM:00/thermal_zone/temp";

[[gnu::noreturn]] [[gnu::cold]] static auto initialize() -> void {
  const u64 data = 0;

  {
    auto fd = open(persist, OpenFlags().write().excl(), 0666);
    write(fd, &data, sizeof(data));
    close(fd);
  }

  static constexpr const char msg[] = "Starting";
  write(1, msg, sizeof(msg));
  exit();
}

static auto persist_update() -> void {
  auto fd = open(persist, OpenFlags().rdwr(), 0);
  if (fd < 0) [[unlikely]] {
    initialize();
  }
  u64 saved;
  read(fd, &saved, sizeof(saved));

  if (current > saved) [[unlikely]] {
    saved = current;
    lseek(fd, 0L, seek::set);
    write(fd, &saved, sizeof(saved));
  } else {
    current = saved;
  }

  close(fd);
}

static auto checktype() -> bool {
  static constexpr char zonetype[]             = "/sys/devices/LNXSYSTM:00/LNXSYBUS:01/LNXTHERM:00/thermal_zone/type";
  static constexpr char expected[]             = "acpitz";
  auto                  fd                     = open(zonetype, OpenFlags().read(), 0);
  buffer[read(fd, buffer, sizeof(buffer) - 1)] = '\0';
  close(fd);
  for (u64 i = 0; i < 4; i++) {
    if (buffer[i] != expected[i]) [[unlikely]] {
      return false;
    }
  }
  return true;
}

[[noreturn]]
static inline auto maxtemp() -> void {
  if (!checktype()) [[unlikely]] {
    static constexpr char msg[] = "Goddamnit";
    write(1, msg, sizeof(msg) - 1);
    exit();
  }
  current = parse_from(zonetemp) / 1000;
  persist_update();

  bptr    = lib::writenum(current, bptr);
  *bptr++ = ' ';
  write(1, buffer, bptr - buffer);
  exit();
}
} // namespace maxtemp
