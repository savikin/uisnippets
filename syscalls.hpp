#pragma once

using u64 = __UINT64_TYPE__;
using u32 = __UINT32_TYPE__;
using u16 = __UINT16_TYPE__;
using u8  = __UINT8_TYPE__;
using i64 = __INT64_TYPE__;
using i32 = __INT32_TYPE__;
using i16 = __INT16_TYPE__;
using i8  = __INT8_TYPE__;

static_assert(sizeof(i8) == sizeof(u8) && sizeof(i8) == 1);
static_assert(sizeof(i16) == sizeof(u16) && sizeof(i16) == 2);
static_assert(sizeof(i32) == sizeof(u32) && sizeof(i32) == 4);
static_assert(sizeof(i64) == sizeof(u64) && sizeof(i64) == 8);

#define ARRCOUNT(x) (sizeof((x)) / sizeof((x)[0]))
#define SIZEDSTR(x) (x), (ARRCOUNT(x) - 1)

static constexpr const u64 HUGEPAGE = 1UL << 20;

struct sockaddr {
    u16 sin_family; /* AF_INET */
    u16 sin_port;   /* IPv4 port */
    u32 sin_addr;   /* IPv4 address */
    u8  pad[8];
};
static_assert(sizeof(sockaddr) == 16);

union sigval {       /* Data passed with notification */
    int   sival_int; /* Integer value */
    void *sival_ptr; /* Pointer value */
};
struct siginfo_t {
    int    si_signo;  /* Signal number */
    int    si_code;   /* Signal code */
    int    si_pid;    /* Sending process ID */
    int    si_uid;    /* Real user ID of sending process */
    void  *si_addr;   /* Memory location which caused fault */
    int    si_status; /* Exit value or signal */
    sigval si_value;  /* Signal value */
};

struct OpenFlags final {
    const i64 value{0};

    [[nodiscard]] consteval auto read() const -> OpenFlags { return OpenFlags(value); }
    [[nodiscard]] consteval auto write() const -> OpenFlags { return OpenFlags(value | 1); }
    [[nodiscard]] consteval auto rdwr() const -> OpenFlags { return OpenFlags(value | 2); }
    [[nodiscard]] consteval auto creat() const -> OpenFlags { return OpenFlags(value | 100); }
    [[nodiscard]] consteval auto excl() const -> OpenFlags { return OpenFlags(value | 200); }
    [[nodiscard]] consteval auto cloexec() const -> OpenFlags { return OpenFlags(value | 2000000); }
};

enum AF : u16 { AF_INET = 2 }; // NOLINT(performance-enum-size)
enum SOCKET_TYPE : int {
  SOCK_STREAM    = 1,
  SOCK_DGRAM     = 2,
  SOCK_RAW       = 3,
  SOCK_RDM       = 4,
  SOCK_SEQPACKET = 5,
  SOCK_DCCP      = 6,
  SOCK_PACKET    = 10,
  SOCK_CLOEXEC   = 02000000,
  SOCK_NONBLOCK  = 00004000
};
enum PROTO : int { // NOLINT(performance-enum-size)
  PROTO_UNKNOWN = 0,
  PROTO_TCP     = 6,
};

enum class seek { set = 0, cur = 1, end = 2 }; // NOLINT(performance-enum-size)

extern "C" {
/*
 * Userspace arguments:
 * %rdi, %rsi, %rdx, %rcx, %r8, %r9.
 * Kernelspace arguments:
 * %rdi, %rsi, %rdx, %r10, %r8, %r9.
 *
 * in extended asm:
 * D S d… with more than 3 arguments
 * you will use local register variables
 * https://gcc.gnu.org/onlinedocs/gcc/Local-Register-Variables.html
 *
 * like so:
 * register void *r10reg asm ("r10");
 * */
// Assembly defined
extern void            _start();
// Keep the C signature
[[gnu::noreturn]] auto main(void *rsp) -> void;
}

asm(R"(
.globl _start
    .type _start, @function
    _start:
    movq %rsp, %rdi
    call main
)");

/* syscalls section
 * */
template <i64 callnum>
[[gnu::always_inline]] static inline auto gen_syscall(auto a1, auto a2, auto a3, auto s4, auto s5, auto s6) -> i64 {
  register auto a4 asm("r10") = s4;
  register auto a5 asm("r8")  = s5;
  register auto a6 asm("r9")  = s6;
  volatile i64  rax           = callnum;
  asm volatile("syscall" : "+a"(rax) : "D"(a1), "S"(a2), "d"(a3), "r"(a4), "r"(a5), "r"(a6) : "memory", "rcx", "r11");
  return rax;
}

template <i64 callnum> [[gnu::always_inline]] static inline auto gen_syscall() -> i64 {
  return gen_syscall<callnum>(0ULL, 0ULL, 0ULL, 0ULL, 0ULL, 0ULL);
}
template <i64 callnum> [[gnu::always_inline]] static inline auto gen_syscall(auto a1) -> i64 {
  return gen_syscall<callnum>(a1, 0ULL, 0ULL, 0ULL, 0ULL, 0ULL);
}
template <i64 callnum> [[gnu::always_inline]] static inline auto gen_syscall(auto a1, auto a2) -> i64 {
  return gen_syscall<callnum>(a1, a2, 0ULL, 0ULL, 0ULL, 0ULL);
}
template <i64 callnum> [[gnu::always_inline]] static inline auto gen_syscall(auto a1, auto a2, auto a3) -> i64 {
  return gen_syscall<callnum>(a1, a2, a3, 0ULL, 0ULL, 0ULL);
}
template <i64 callnum>
[[gnu::always_inline]] static inline auto gen_syscall(auto a1, auto a2, auto a3, auto a4) -> i64 {
  return gen_syscall<callnum>(a1, a2, a3, a4, 0ULL, 0ULL);
}
template <i64 callnum>
[[gnu::always_inline]] static inline auto gen_syscall(auto a1, auto a2, auto a3, auto a4, auto a5) -> i64 {
  return gen_syscall<callnum>(a1, a2, a3, a4, a5, 0ULL);
}

static inline auto read(i64 fd, void *dest, u64 size) { return gen_syscall<0>(fd, dest, size); }
static inline auto write(i64 fd, const void *src, i64 size) { return gen_syscall<1>(fd, src, size); }
static inline auto open(const char *pathname, OpenFlags flags, i64 mode) -> i64 {
  return gen_syscall<2>(pathname, flags, mode);
}
static inline auto close(i64 fd) -> i64 { return gen_syscall<3>(fd); }
static inline auto stat(const char *pathname, void *statbuf) -> i64 { return gen_syscall<4>(pathname, statbuf); }
static inline auto lseek(i64 fd, i64 offset, seek whence) -> i64 { return gen_syscall<8>(fd, offset, whence); };

static inline auto pipe(int *pipefd) -> i64 { return gen_syscall<22>(pipefd); }
static inline auto dup2(i64 oldfd, i64 newfd) -> i64 { return gen_syscall<33>(oldfd, newfd); }

static inline auto socket(i64 domain, int type, int protocol) -> i64 {
  return gen_syscall<41>(domain, type, protocol);
};
static inline auto connect(i64 sockfd, const struct sockaddr *addr, u64 addrlen) -> i64 {
  return gen_syscall<42>(sockfd, addr, addrlen);
}
static inline auto sendto(int socket, const void *message, u64 length, int flags, const struct sockaddr *dest_addr,
                          u64 dest_len) -> i64 {
  return gen_syscall<44>(socket, message, length, flags, dest_addr, dest_len);
}
static inline auto recvfrom(int socket, void *buffer, u64 length, int flags, struct sockaddr *address, u64 *address_len)
    -> i64 {
  return gen_syscall<45>(socket, buffer, length, flags, address, address_len);
}
static inline auto fork() -> i64 { return gen_syscall<57>(); }

[[gnu::noreturn]]
static inline auto execve(const char *pathname, const char *const argv[], const char *const envp[]) -> i64 {
  gen_syscall<59>(pathname, argv, envp);
  __builtin_trap();
}
static inline auto mkdir(const char *pathname, i64 mode) -> i64 { return gen_syscall<83>(pathname, mode); }

static inline auto symlink(const char *target, const char *linkpath) -> i64 {
  return gen_syscall<88>(target, linkpath);
};

static constexpr i64 WEXITED = 4;
static inline auto   waitid(i64 idtype, i64 id, siginfo_t *infop, i64 options, void *rusage) -> i64 {
  return gen_syscall<247>(idtype, id, infop, options, rusage);
}

[[gnu::noreturn]] static inline auto exit() -> void {
  gen_syscall<231>(0);
  __builtin_trap();
};

namespace globals {
static char  buffer[HUGEPAGE];
static char *bptr = buffer;
}; // namespace globals
