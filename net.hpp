#pragma once

#include "syscalls.hpp"

#include "lib.hpp"

namespace net {
using namespace lib;
using namespace globals;

static u64 tx_old;
static u64 tx_new;
static u64 rx_old;
static u64 rx_new;

static constexpr const char persist[] = "/home/angel/.cache/waybar-net-status";
static constexpr const char rxpath[]  = "/sys/class/net/wlp0s20f3/statistics/rx_bytes";
static constexpr const char txpath[]  = "/sys/class/net/wlp0s20f3/statistics/tx_bytes";

[[gnu::noreturn]] [[gnu::cold]] static auto initialize() -> void {
  u64 data[2] = {0, 0};

  auto fd     = open(persist, OpenFlags().write().excl(), 0666);
  write(fd, data, sizeof(data));
  close(fd);

  static constexpr const char msg[] = "Starting";
  write(1, msg, sizeof(msg));
  exit();
}

static auto persist_update() -> void {
  auto fd = open(persist, OpenFlags().rdwr(), 0);
  if (fd < 0) [[unlikely]] {
    initialize();
  }
  u64 data[2];
  read(fd, data, sizeof(data));
  rx_old  = data[0];
  tx_old  = data[1];
  data[0] = rx_new;
  data[1] = tx_new;
  lseek(fd, 0L, seek::set);
  write(fd, &data, sizeof(data));
  close(fd);
}

[[noreturn]]
static inline auto net() -> void {
  rx_new = parse_from(rxpath);
  tx_new = parse_from(txpath);
  persist_update();

  bptr    = lib::print_bytes(rx_new, bptr);
  *bptr++ = ' ';
  bptr    = lib::print_bytes(tx_new, bptr);
  *bptr++ = ' ';
  bptr    = lib::print_bytes(rx_new - rx_old, bptr);
  *bptr++ = ' ';
  bptr    = lib::print_bytes(tx_new - tx_old, bptr);
  write(1, buffer, bptr - buffer);

  exit();
}
} // namespace net
