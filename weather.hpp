#pragma once

#include "lib.hpp"
#include "syscalls.hpp"

namespace weather {
struct state {
    i64 epoch_sec;
    i64 temp_whole;
    i64 temp_fract;
};

[[gnu::noreturn]]
static inline void run_curl() {
  static constexpr char        arg0[] = "curl";
  static constexpr char        arg1[] = "--silent";
  static constexpr char        arg2[] = "http://127.0.0.5:80/";
  // static constexpr char        arg2[] = "https://api.open-meteo.com/v1/forecast?"
  //                                       "latitude=59.5615&longitude=30.1831&current=temperature_2m";
  static constexpr const char *argv[] = {arg0, arg1, arg2, nullptr};

  execve("/usr/bin/curl", argv, nullptr);
}

static inline void parse_temperature(state *state, const char *buffer) {
  auto *p = buffer + 17;
  if (!lib::is_digit(*p)) {
    return;
  }
  state->temp_whole = (i64)lib::parse_uint64(p);
  while (lib::is_digit(*p)) {
    p++;
  }
  if (*p != '.') {
    return;
  }
  p++;
  if (!lib::is_digit(*p)) {
    return;
  }
  state->temp_fract = (i64)lib::parse_uint64(p);
}

static inline void parse(state *state, const char *buffer) {
  static constexpr char temp[]  = R"("temperature_2m":)";
  static constexpr i64  temp_sz = sizeof(temp) - 1;
  if (lib::memequal(buffer, temp, sizeof(temp) - 1)) {
    parse_temperature(state, buffer);
  }
}

// 59°56′15″N
// 30°18′31″E
// curl "https://api.open-meteo.com/v1/forecast?latitude=59.5615&longitude=30.1831&current=temperature_2m"
// {"latitude":59.5585,"longitude":30.175934,"generationtime_ms":0.01895427703857422,
//    "utc_offset_seconds":0,"timezone":"GMT","timezone_abbreviation":"GMT",
//    "elevation":79.0,"current_units":{"time":"iso8601","interval":"seconds","temperature_2m":"°C"},
//  "current":{"time":"2024-10-17T21:30","interval":900,"temperature_2m":5.8}}%
[[gnu::noreturn]]
static inline void weather() {
  lib::Cli cli{};

  auto pid = fork();
  if (pid == 0) {
    cli.child();
    run_curl();
  } else {
    cli.parent();
    cli.in.close_all();

    static char buffer[4096];
    for (;;) {
      siginfo_t sig;
      lib::memclear(&sig, sizeof(sig));

      auto ret = waitid(0, -1, &sig, WEXITED, nullptr);
      if (ret != 0) {
        __builtin_trap();
      }
      if (sig.si_pid == pid) {
        break;
      }
    }
    const auto sz = read(cli.out.rd, buffer, sizeof(buffer) - 1);
    buffer[sz]    = '\0';

    state state{};
    for (i64 i = 0; i < sz; ++i) {
      parse(&state, buffer);
    }
  }
  exit();
}
} // namespace weather
