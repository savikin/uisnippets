#pragma once

#include "lib.hpp"
#include "syscalls.hpp"

namespace cpulevel {
using namespace lib;
using namespace globals;

enum class variables : u8 { BIAS, EPP, GOVERN, BOOST, NPROCS };

enum class BIAS : u8 { BORKED, S15, S0 };
enum class EPP : u8 { BORKED, POWER, PERFORMANCE };
enum class GOVERN : u8 { BORKED, POWERSAVE, PERFORMANCE };
enum class BOOST : u8 { BORKED, OFF, ON };
enum class NPROCS : u8 { BORKED, S1, HUH };
enum class SUMUP : u8 { BORKED, POWER, PERF };

static struct {
    BIAS   bias;
    GOVERN govern;
    BOOST  boost;
    EPP    epp;
    NPROCS nprocs;
    SUMUP  sumup;
} state;

[[gnu::cold]] [[gnu::noreturn]] static auto fail(const char *msg, i64 size) -> void {
  write(1, msg, size);
  exit();
}

template <u64 N> [[gnu::cold]] [[gnu::noreturn]] static auto fail(const char (&msg)[N]) -> void { fail(msg, N); }

static auto sumup() -> void {
  if (state.bias == BIAS::S15 && state.epp == EPP::POWER && state.govern == GOVERN::POWERSAVE) [[likely]] {
    state.sumup = SUMUP::POWER;
  } else if (state.bias == BIAS::S0 && state.epp == EPP::PERFORMANCE && state.govern == GOVERN::PERFORMANCE) {
    state.sumup = SUMUP::PERF;
  } else [[unlikely]] {
    state.sumup = SUMUP::BORKED;
  }
}

static auto nprocs() -> void {
  auto fd                                      = open("/sys/devices/system/cpu/possible", OpenFlags().read(), 000);
  buffer[read(fd, buffer, sizeof(buffer) - 1)] = '\0';
  close(fd);
  if (buffer[0] == '0' && buffer[1] == '\n') {
    state.nprocs = NPROCS::S1;
  } else {
    state.nprocs = NPROCS::HUH;
  }
}

template <variables type> static auto check() -> void {
  i64 fd;
  if constexpr (type == variables::BIAS) {
    fd = open("/sys/devices/system/cpu/cpu0/power/energy_perf_bias", OpenFlags().read(), 000);
  } else if constexpr (type == variables::EPP) {
    fd = open("/sys/devices/system/cpu/cpufreq/policy0/energy_performance_preference", OpenFlags().read(), 000);
  } else if constexpr (type == variables::GOVERN) {
    fd = open("/sys/devices/system/cpu/cpufreq/policy0/scaling_governor", OpenFlags().read(), 000);
  } else if constexpr (type == variables::BOOST) {
    fd = open("/sys/devices/system/cpu/intel_pstate/no_turbo", OpenFlags().read(), 000);
  } else if constexpr (type == variables::NPROCS) {
    nprocs();
    return;
  }

  if (fd < 0) [[unlikely]] {
    if constexpr (type == variables::BIAS) {
      fail("BIAS FD FAIL");
    } else if constexpr (type == variables::EPP) {
      fail("EPP FD FAIL");
    } else if constexpr (type == variables::GOVERN) {
      fail("GOVERN FD FAIL");
    } else if constexpr (type == variables::BOOST) {
      fail("BOOST FD FAIL");
    }
  }

  buffer[read(fd, buffer, sizeof(buffer) - 1)] = '\0';
  close(fd);

  if constexpr (type == variables::BIAS) {
    if (buffer[0] == '1' && buffer[1] == '5') [[likely]] {
      state.bias = BIAS::S15;
    } else if (buffer[0] == '0') {
      state.bias = BIAS::S0;
    } else [[unlikely]] {
      fail("BIAS BORKED");
    }
  } else if constexpr (type == variables::EPP) {
    if (buffer[0] == 'p' && buffer[1] == 'o' && buffer[2] == 'w') [[likely]] {
      state.epp = EPP::POWER;
    } else if (buffer[0] == 'p' && buffer[1] == 'e' && buffer[2] == 'r') {
      state.epp = EPP::PERFORMANCE;
    } else [[unlikely]] {
      fail("EPP BORKED");
    }
  } else if constexpr (type == variables::GOVERN) {
    if (buffer[0] == 'p' && buffer[1] == 'o' && buffer[2] == 'w') [[likely]] {
      state.govern = GOVERN::POWERSAVE;
    } else if (buffer[0] == 'p' && buffer[1] == 'e' && buffer[2] == 'r') {
      state.govern = GOVERN::PERFORMANCE;
    } else [[unlikely]] {
      fail("GOVERN BORKED");
    }
  } else if constexpr (type == variables::BOOST) {
    if (buffer[0] == '1') [[likely]] {
      state.boost = BOOST::OFF;
    } else if (buffer[0] == '0') {
      state.boost = BOOST::ON;
    } else [[unlikely]] {
      fail("BOOST BORKED");
    }
  }
}

[[noreturn]]
static inline auto cpulevel() -> void {
  check<variables::BOOST>();
  check<variables::BIAS>();
  check<variables::EPP>();
  check<variables::GOVERN>();
  check<variables::NPROCS>();

  sumup();

  const auto sum   = state.sumup;
  const auto boost = state.boost;
  bptr             = buffer;
  if (sum == SUMUP::POWER) [[likely]] {
    bptr = lib::write(bptr, SIZEDSTR("🛋️"));
  } else if (sum == SUMUP::PERF && boost == BOOST::OFF) {
    bptr = lib::write(bptr, SIZEDSTR("🐇"));
  } else if (sum == SUMUP::PERF && boost == BOOST::OFF) {
    bptr = lib::write(bptr, SIZEDSTR("🐇🥦"));
  } else if (sum == SUMUP::PERF && boost == BOOST::ON) {
    bptr = lib::write(bptr, SIZEDSTR("💥"));
  } else if (sum == SUMUP::PERF && boost == BOOST::ON) {
    bptr = lib::write(bptr, SIZEDSTR("💥🥦"));
  } else {
    bptr = lib::write(bptr, SIZEDSTR("huh?"));
  }

  if (!(state.nprocs == NPROCS::S1)) [[unlikely]] {
    bptr = lib::write(bptr, SIZEDSTR("‼"));
  }

  write(1, buffer, bptr - buffer);
  exit();
}
} // namespace cpulevel
