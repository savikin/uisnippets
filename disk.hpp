#pragma once

#include "lib.hpp"
#include "syscalls.hpp"

namespace disk {
using namespace lib;
using namespace globals;
static u64 rx;
static u64 tx;
static u64 rx_old;
static u64 tx_old;

static constexpr auto to_bytes(u64 sectors) -> u64 { return sectors * 512; }

static constexpr const char persist[]  = "/home/angel/.cache/waybar-disk-status";
static constexpr const char statfile[] = "/sys/block/nvme0n1/stat";

static constexpr auto skip_number(char *&c) {
  for (; !(is_digit(*c)); c++) {
  }
  for (; is_digit(*c); c++) {
  }
}

[[gnu::noreturn]] [[gnu::cold]] static auto initialize() -> void {
  u64 data[2] = {0, 0};

  auto fd     = open(persist, OpenFlags().write().excl(), 0666);
  write(fd, data, sizeof(data));
  close(fd);

  static constexpr const char msg[] = "Starting";
  write(1, msg, sizeof(msg));
  exit();
}

static auto retrieve_stats() -> void {
  auto fd = open(statfile, OpenFlags().read(), 0);
  read(fd, buffer, sizeof(buffer));
  close(fd);
  char *c = buffer;
  skip_number(c); // read IOs
  skip_number(c); // read merges

  for (; !(is_digit(*c)); c++) {
  }
  rx = to_bytes(parse_uint64(c));
  skip_number(c);

  skip_number(c); // read ticks
  skip_number(c); // write IOs
  skip_number(c); // write merges
  for (; !(is_digit(*c)); c++) {
  }
  tx = to_bytes(parse_uint64(c));
}

static auto persist_update() -> void {
  auto fd = open(persist, OpenFlags().rdwr(), 0);
  if (fd < 0) [[unlikely]] {
    initialize();
  }
  u64 data[2];
  read(fd, data, sizeof(data));
  rx_old  = data[0];
  tx_old  = data[1];
  data[0] = rx;
  data[1] = tx;
  lseek(fd, 0L, seek::set);
  write(fd, &data, sizeof(data));
  close(fd);
}

[[noreturn]]
static inline auto disk() -> void {
  retrieve_stats();
  persist_update();

  bptr           = lib::print_bytes(rx, bptr);
  *bptr++        = ' ';
  bptr           = lib::print_bytes(tx, bptr);
  *bptr++        = ' ';

  const auto rxd = rx - rx_old;
  const auto txd = tx - tx_old;

  bptr           = lib::print_bytes(rxd, bptr);
  *bptr++        = ' ';
  bptr           = lib::print_bytes(txd, bptr);

  write(1, buffer, bptr - buffer);
  exit();
}
} // namespace disk
