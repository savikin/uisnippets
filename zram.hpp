#pragma once

#include "lib.hpp"
#include "syscalls.hpp"

[[gnu::noreturn]]
static inline void zram() {
  using namespace lib;
  static constexpr auto skip = [](char **p) constexpr {
    while (!is_digit(**p)) {
      (*p)++;
    }
    while (is_digit(**p)) {
      (*p)++;
    }
    while (!is_digit(**p)) {
      (*p)++;
    }
  };

  using namespace globals;
  readfile("/sys/block/zram0/mm_stat", buffer);
  skip(&bptr);
  skip(&bptr);

  u64 bytes = parse_uint64(bptr);
  bptr      = buffer;

  u64 mb    = bytes / 1024 / 1024;

  if (mb == 0) {
    exit();
  }

  bptr = lib::writenum(mb, bptr);
  write(1, buffer, bptr - buffer);
  exit();
}
