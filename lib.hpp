#pragma once

#include "syscalls.hpp"

namespace lib {

static constexpr void waitid() {}
static constexpr void memclear(void *ptr, i64 size) {
  auto *p = (char *)ptr;
  for (i64 i = 0; i < size; ++i) {
    p[i] = 0;
  }
}
static constexpr auto memequal(const void *p1, const void *p2, u64 size) -> bool {
  auto *c1 = (const char *)(p1);
  auto *c2 = (const char *)(p2);
  for (u64 i = 0; i < size; ++i) {
    if (c1[i] != c2[i]) {
      return false;
    }
  }
  return true;
}

static constexpr auto write(char *dest, const char *src, i64 N) {
  for (i64 i = 0; i < N;) {
    *dest++ = src[i++];
  }
  return dest;
}

static constexpr auto writenum(u64 val, char *const buffer) -> char * {
  char *p = buffer;

  do {
    *p++  = '0' | (char)(val % 10);
    val  /= 10;
  } while (val);
  *p          = '\0';

  char *left  = buffer;
  char *right = p - 1;
  while (left < right) {
    char c = *right;
    *right = *left;
    *left  = c;

    ++left;
    --right;
  }
  return p;
}

static constexpr auto print_bytes(u64 bytes, char *const output) -> char * {
  static constexpr u64 mb = 1024UL * 1024UL;
  static constexpr u64 kb = 1024UL;

  if (bytes > mb) {
    auto *out = writenum(bytes / mb, output);
    if (bytes < 10 * mb) {
      *out++ = '.';
      out    = writenum((bytes - ((bytes / mb) * mb)) / (100 * kb), out);
    }
    *out++ = 'm';
    return out;
  }
  if (bytes > kb) {
    auto *out = writenum(bytes / kb, output);
    *out++    = 'k';
    return out;
  }
  auto *out = writenum(bytes, output);
  return out;
}
static inline auto readchar(const char *const filepath) -> char {
  char       c;
  const auto fd = open(filepath, OpenFlags().read(), 0);
  read(fd, &c, 1);
  close(fd);
  return c;
}

template <u64 N> static inline auto readfile(const char *const path, char (&databuffer)[N]) {
  const auto fd    = open(path, OpenFlags().read(), 0);
  const auto size  = read(fd, databuffer, sizeof(databuffer) - 1);
  databuffer[size] = '\0';
  close(fd);
  return size;
}

static constexpr auto is_digit(char c) { return ('0' <= c && c <= '9'); }

static inline auto parse_uint64(const char *databuffer) {
  u64         retval = 0;
  const char *c      = databuffer;
  while (!is_digit(*c)) {
    ++c;
  }
  while (is_digit(*c)) {
    retval = retval * 10 + (*c++ & 0xf);
  }
  return retval;
}

static inline auto parse_from(const char *path) -> u64 {
  char buffer[128];
  readfile(path, buffer);
  return parse_uint64(buffer);
}

[[maybe_unused]]
static void memmove(void *dest, const void *src, i64 n) {
  char       *d = (char *)(dest);
  const char *s = (const char *)(src);

  if (d < s) {
    for (; n; n--) {
      *d++ = *s++;
    }
  } else {
    while (n) {
      n--, d[n] = s[n];
    }
  }
}

struct Pipes {
    int rd;
    int wr;

    Pipes() {
      rd = -1;
      wr = -1;
      if (0 != pipe(&rd)) {
        __builtin_trap();
      }
    }

    void init() { pipe(&rd); }
    void close_read() {
      if (rd != -1) {
        close(rd);
        rd = -1;
      }
    }
    void close_write() {
      if (wr != -1) {
        if (0 != close(wr)) {
          __builtin_trap();
        }
        wr = -1;
      }
    }
    void close_all() {
      close_read();
      close_write();
    }
    ~Pipes() { close_all(); }
};

struct Cli {
    Pipes in;
    Pipes out;
    Pipes err;

    void child() {
      dup2(in.rd, 0);
      dup2(out.wr, 1);
      dup2(err.wr, 2);

      in.close_all();
      out.close_all();
      err.close_all();
    }
    void parent() {
      in.close_read();
      out.close_write();
      err.close_write();
    }
};

}; // namespace lib
