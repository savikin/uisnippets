#pragma once

#include "syscalls.hpp"

#include "lib.hpp"

namespace lo {
using namespace lib;
using namespace globals;

static u64 tx_old;
static u64 tx_new;

static constexpr const char persist[] = "/home/angel/.cache/waybar-lo-status";

[[gnu::noreturn]] [[gnu::cold]] static auto initialize() -> void {
  u64 data[2] = {0, 0};

  auto fd     = open(persist, OpenFlags().write().excl(), 0666);
  write(fd, data, sizeof(data));
  close(fd);

  static constexpr const char msg[] = "Starting";
  write(1, msg, sizeof(msg));
  exit();
}

static auto persist_update() -> void {
  auto fd = open(persist, OpenFlags().rdwr(), 0);
  if (fd < 0) [[unlikely]] {
    initialize();
  }
  read(fd, &tx_old, sizeof(tx_old));
  lseek(fd, 0L, seek::set);
  write(fd, &tx_new, sizeof(tx_new));
  close(fd);
}

[[noreturn]]
static inline auto lo() -> void {
  tx_new = parse_from("/sys/class/net/lo/statistics/tx_bytes");
  persist_update();

  bptr    = lib::print_bytes(tx_new, bptr);
  *bptr++ = ' ';
  bptr    = lib::print_bytes(tx_new - tx_old, bptr);
  write(1, buffer, bptr - buffer);
  exit();
}
} // namespace lo
